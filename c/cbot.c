#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>
  
// OJO!! Borrar los objetos cJSON

#include "cJSON.h"

// CONSTANTES DEL PID
#define PID 		0
#define REF_ANGLE 	10

#define K_P	0.4619
#define K_I	-0.5779
#define K_D	0.2009

#define NUM_TRAMOS  60
#define TICKS_V 5
#define DEBUG_JESUS 0 
#define DEBUG_JOSE  0
#define DEBUG_MSG_TO_SERVER 0 

#define MARGEN_FRENADO 50

#define VIEW_TO_SWITCH 4
#define PIECES_BEFORE_SWITCH 1
#define PIECES_OFFSET 2     // Empezamos a mirar x piezas detras de la actual

static cJSON *ping_msg();
static cJSON *join_msg(char *bot_name, char *bot_key);
static cJSON *throttle_msg(double throttle);
static cJSON *left(void);
static cJSON *right(void);
static cJSON *turbo_msg(void);
static cJSON *make_msg(char *type, cJSON *msg);
static cJSON *read_msg(int fd);
static void write_msg(int fd, cJSON *msg);
static unsigned mod(int a, unsigned b); 


static void error(char *fmt, ...)
{
    char buf[BUFSIZ];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);

    if (errno)
        perror(buf);
    else
        fprintf(stderr, "%s\n", buf);

    exit(1);
}

static int connect_to(char *hostname, char *port)
{
    int status;
    int fd;
    char portstr[32];
    struct addrinfo hint;
    struct addrinfo *info;

    memset(&hint, 0, sizeof(struct addrinfo));
    hint.ai_family = PF_INET;
    hint.ai_socktype = SOCK_STREAM;

    sprintf(portstr, "%d", atoi(port));

    status = getaddrinfo(hostname, portstr, &hint, &info);
    if (status != 0) error("failed to get address: %s", gai_strerror(status));

    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) error("failed to create socket");

    status = connect(fd, info->ai_addr, info->ai_addrlen);
    if (status < 0) error("failed to connect to server");

    freeaddrinfo(info);
    return fd;
}

static void log_message(char *msg_type_name, cJSON *msg)
{
    cJSON *msg_data;

    if (!strcmp("join", msg_type_name)) {
        puts("Joined");
    } else if (!strcmp("joinRace", msg_type_name)) {
        puts("Joined to race");
    } else if (!strcmp("gameStart", msg_type_name)) {
        puts("Race started");
    } else if (!strcmp("crash", msg_type_name)) {
        puts("Someone crashed");
    } else if (!strcmp("gameEnd", msg_type_name)) {
        puts("Race ended");
    } else if (!strcmp("error", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        if (msg_data == NULL)
            puts("Unknown error");
        else
            printf("ERROR: %s\n", msg_data->valuestring);
    }
}

/* Función que calcula el módulo de un número sin devolver números negativos */
static unsigned mod(int a, unsigned b) {
    return (a >= 0 ? a % b : b - (-a) % b);
}


/* función que impreme los valores de un array 
   Uso:

   print_array_int(array , sizeof(array )/sizeof(array[0]));
*/
void print_array_int(int a[], size_t a_size) {
    int i;
    for(i=0; i< a_size;i++)
       printf("Array value %d: %d\n", i, a[i]); 
}
    
void print_array_float(float a[], size_t a_size) {
    int i;
    for(i=0; i< a_size;i++)
       printf("Array value %d: %f\n", i, a[i]);
}


int main(int argc, char *argv[])
{
    int sock; 
    cJSON *json;
    
    /********************* DECLARACION DE VARIABLES ****************************/
        // Informacion de las piezas
        float *arrayAngles, *arrayLengthAndRadius;
        int *arrayCurves, *arraySwitches;

        float *arrayDistUntilChange; 
        int *arrayChangeIndex, *arrayChangeCurve;
        
        float *arrayLanes;
        int nLanes;
	float distanceLane;

        int nPieces;

	float factorTurbo = 0.0, dTurbo = 0;
	int ticksTurbo = 0, turboAvailable = 0, usaTurbo = 0;
        
        // Informacion para la situacion actual
        float pieceDistance = 0, lastPieceDistance = 0; 
        float carAngle = 0, lastCarAngle = 0, last2CarAngle = 0;
        int pieceIndex = 0, lastPieceIndex = 0;
        int laneStart = 0, laneEnd = 0;
        int lap = 0; 
        float sumAngle = 0;  
 
        // Estado del coche
        enum CarStatus {speedUp, speedDown, speedSame, analyze} status = speedUp, lastStatus = speedUp;
        
        // Variables para medir aceleracion, velocidad...
        int t0 = 0, t1 = 0, time = 0;
        float distance = 0, a_car, a_roz = 0.0001;
	//float a_total;
        float lastSpeed = 0, last2Speed = 0, speed = 0;
	float v0, vf;
        int gameTick = 0, lastGameTick = 0, last2GameTick = 0, nextIndex; 
        
        // para calculo de rozamiento
        int computedRoz = 0, computingRoz = 0;
        float betterR;       
          
        // Pedal de aceleracion (util para frenar/acelerar)
        float T = 0, lastT = 0;
        //float maxT, minT, difT;     // Ajuste del pedal al llegar a curvas
        //int tBrake, ticksBrake, piecesViewCount;
    /***************************************************************************/
 
    if (argc != 5) error("Usage: bot host port botname botkey\n");

    sock = connect_to(argv[1], argv[2]);


    json = join_msg(argv[3], argv[4]);
    write_msg(sock, json);
    cJSON_Delete(json);

   while ((json = read_msg(sock)) != NULL) 
   {
        cJSON *msg, *msg_type, *data;
        char *msg_type_name;

       char * json_text = cJSON_Print(json);
       // Imprime la linea los mensajes del servidor en formato JSON
       if (DEBUG_JOSE) printf("%s\n", json_text);
       
        msg_type = cJSON_GetObjectItem(json, "msgType");
        data = cJSON_GetObjectItem(json, "data");
        
        if (msg_type == NULL)
            error("missing msgType field");

        msg_type_name = msg_type->valuestring;
        if (!strcmp("carPositions", msg_type_name)) 
        {
           //  msg = throttle_msg(0.5);

	/* ================= INICIALIZACION DE VARIABLES ================== 

		Obtenemos los datos del mensaje carPositions
	*/
            gameTick = lastGameTick;
            if(cJSON_GetObjectItem(json, "gameTick"))
              gameTick = cJSON_GetObjectItem(json, "gameTick")->valueint;

            if (DEBUG_JESUS) printf("GAMETICKS leido: %d\n", gameTick);

            data = cJSON_GetArrayItem(data, 0);
            cJSON* _piecePosition = cJSON_GetObjectItem(data, "piecePosition");
            cJSON* _lane =  cJSON_GetObjectItem(_piecePosition, "lane");
            if (DEBUG_JESUS) printf("Accedido a piecePosition y lane\n");

            laneStart = cJSON_GetObjectItem(_lane, "startLaneIndex")->valueint;
            laneEnd = cJSON_GetObjectItem(_lane, "endLaneIndex")->valueint;
            lap = cJSON_GetObjectItem(_piecePosition, "lap")->valueint;
            pieceDistance = (float) cJSON_GetObjectItem(_piecePosition, "inPieceDistance")->valuedouble;
            pieceIndex = cJSON_GetObjectItem(_piecePosition, "pieceIndex")->valueint;
            carAngle = (float) cJSON_GetObjectItem(data, "angle")->valuedouble;
            if (DEBUG_JESUS || DEBUG_JOSE) printf("Accedido a valores numericos: lane%d, laneEnd%d lap%d, pDist%f, pIndex%d, carAngle%f, status(%d), indexChange(%d)\n", laneStart, laneEnd, lap, pieceDistance, pieceIndex, carAngle, status, arrayChangeIndex[pieceIndex]);            

	    nextIndex = mod(pieceIndex+1, nPieces);

	    if (DEBUG_JESUS) printf("PEDAL -> %f \n\n", lastT);
	/* ================= FIN Inicializacion carPositions ===================== */


	/* ================ CALCULO DE VELOCIDAD y ROZAMIENTO =================== */	

            // Calcular velocidad como diferencia de posiciones en un tick
            if(pieceIndex != lastPieceIndex)
            {
                float longTramo;

                if(arrayCurves[lastPieceIndex])
                {
		    if (DEBUG_JESUS) printf("cambio de pieza y en curva\n");

		    distanceLane = arrayLanes[laneStart];
                    longTramo = M_PI*arrayAngles[lastPieceIndex]*
 				(arrayLengthAndRadius[lastPieceIndex]-distanceLane) / 180;               		   
                    if(longTramo < 0) longTramo = -longTramo;

		     if(!computedRoz)
                     {
                    	status = analyze;
			if(DEBUG_JESUS) printf("cambio de estado: %d -> analyze(%d) \n", lastStatus, status);

                    	computingRoz = 1;

                    	t0 = gameTick;
                    	t1 = gameTick + TICKS_V;
                    	v0 = pieceDistance + (longTramo - lastPieceDistance);
                     }
 		}
                else
                {
                    longTramo = arrayLengthAndRadius[lastPieceIndex];
                    if (DEBUG_JESUS) printf("cambio de pieza y en recta\n");                
                }
                speed = pieceDistance + (longTramo - lastPieceDistance);
            }
            else    speed = pieceDistance - lastPieceDistance;

	/* ================== FIN CALCULO VELOCIDAD y ROZAMIENTO ======= */

            
            if (DEBUG_JESUS) printf("v=%f \n", speed);            
            
            /* numero de piezas siguientes a considerar segun la velocidad
            piecesViewCount = speed * 0.5;
            if(piecesViewCount > 5)         piecesViewCount = 5;
            else if(piecesViewCount < 1)    piecesViewCount = 1;
            */            





       /*============================ CAMBIO DE CARRIL ==========================
              Necesitamos crear un acumulador de angulos con signo.

       ======================================================================== */

          int i;
          int iMod;
          sumAngle = 0;
          for ( i= 0; i < VIEW_TO_SWITCH; i++) {
              iMod = mod(i+pieceIndex+PIECES_OFFSET, (unsigned int) nPieces);
              sumAngle = sumAngle + arrayAngles[iMod];
          }

          if (DEBUG_JOSE) printf("sumAngle: %f\n", sumAngle);

          iMod = mod(pieceIndex + PIECES_BEFORE_SWITCH, (unsigned int) nPieces);
          if (arraySwitches[iMod]) {
             if (DEBUG_JOSE) printf("arraySwitches %d %d", iMod, arraySwitches[iMod]);
             if (sumAngle < 0) { // giramos a la izquierda
                msg = left();
                if (DEBUG_JOSE) printf("Cambio de carril a la izquierda\n");
                write_msg(sock, msg);
                continue;
             } else {
                msg = right();
                if (DEBUG_JOSE) printf("Cambio de carril a la derecha\n");
                write_msg(sock, msg);
                continue;
             }
          }

       /*==========================================================================*/

	/* ================== AJUSTAMOS EL PEDAL SEGUN ESTADO ============== 
		
		analyze: PRECONDICIONES: ha de estar en una recta de longitud de
					al menos 5 TICKS a velocidad maxima 
			una vez pasados unos ticks, calcula el coef. de roz.
			luego sigue acelerando en linea recta

		speedUp: PRECONDICIONES: la distancia al proximo cambio ha de ser mayor
					que distanciaFrenado+margen

		speedSame: PRECONDICIONES: solo se mantiene en las curvas. tiene que estar
					en una curva. si la velocidad del tick anterior es
					menor que la deseada, aumenta el pedal. en otro caso
					reduce el pedal

		speedDown: PRECONDICIONES: que la distancia a la curva sea menor que la distancia
					de frenado + margen.
	*/

            switch(status)
            {
                case analyze:
			T = 0;
			if(computingRoz  &&  gameTick >= t1)
			{
				computedRoz = 1;
				computingRoz = 0;

				status = speedUp;
				if(DEBUG_JESUS) printf("cambio de estado: %d -> speedUp(%d) \n", lastStatus, status);


			 	time = gameTick - t0;

                    		// Averiguar aceleracion debida al rozamiento
                    		a_roz = (v0 - speed) / time;
                    		if (DEBUG_JESUS) printf("a_roz: d=%f \n", a_roz);
				//return;
			} 
                     	break;
                
                case speedUp:     
                        T = 0.7;
                       
		     if(computedRoz)
		     {
			// Elegir el mejor carril para curvas (menor radio, el de la derecha)
			float relRadius = arrayLanes[laneStart];
			for(i = 0; i < nLanes; i++)
				if(arrayLanes[i] > relRadius) relRadius = arrayLanes[i];

			betterR = arrayLengthAndRadius[ arrayChangeIndex[pieceIndex] ] - relRadius;

			//printf("RADIUS REL: %f, RADIUS TOTAL: %f \n\n", arrayLengthAndRadius[ arrayChangeIndex[pieceIndex]], relRadius); 		
			//printf("BETTER RADIUS: %f, i:%d, index:%d", betterR, pieceIndex, arrayChangeIndex[pieceIndex]);
			
	
			// Calculo de la distancia a la que frenar a tope antes de la curva proxima
			vf = sqrt(a_roz * betterR);
			//printf("v_max:%f \n", vf);
			//return;			

			time = (speed - vf) / a_roz;
			distance = speed*time - a_roz*0.5*time*time;

			if(DEBUG_JESUS) 
			 printf("\n\ndistance: %f, distanceUntilChange: %f, distanceUntilChangeNO_OFFSET: %f \n\n", distance, arrayDistUntilChange[pieceIndex]-pieceDistance, arrayDistUntilChange[pieceIndex]);

			if((distance + MARGEN_FRENADO) >= (arrayDistUntilChange[pieceIndex]-pieceDistance))
			{
                             /*status = speedDown;
			     if(DEBUG_JESUS) 
			printf("cambio de estado: %d -> speedDown(%d) \n", lastStatus, status);
				*/

			    status = speedDown;
			    vf = vf * 0.8;
			    T = 0;                            

			/* Formula menos eficiente de frenado 
			    maxT = T;
                            minT = T / 2;
                            difT = maxT - minT;
                            tBrake = 0;
                            
                            ticksBrake = 20 - (int)(speed);
                            if(ticksBrake < 1)  ticksBrake = 1;
			*/
                        }
		     }
                        break;
            
                case speedDown:
                        
			T = 0;
			if (DEBUG_JESUS) printf("REDUCIENDO... %f, %f\n\n", speed, vf);

			// Si no estamos pisando pedal, adaptar (Usar PDI!!)
			if (DEBUG_JESUS) printf("FRENANDO CON V:%f, HASTA:%f", speed, vf);

			if(speed < vf) {
				status = speedSame;
				 if(DEBUG_JESUS) printf("cambio de estado: %d -> speedSame(%d) \n", lastStatus, status);

			}

			
                        //T = minT + difT * exp(-4*tBrake / ticksBrake);
                        //if(++tBrake > ticksBrake)   status = speedSame;
			
			//status = speedSame;
                        break;
                        
                case speedSame:
                    
			if (DEBUG_JESUS) printf("MANTENIENDO CON V:%f, VF:%f", speed, vf);

			if(PID)
			{
				T = lastT + K_P*(REF_ANGLE - carAngle) + 
					    K_I*(REF_ANGLE - lastCarAngle) + 
					    K_D*(REF_ANGLE - last2CarAngle);
			}
			else
			{
				// Calcular aceleracion infinitesimal
                        	a_car = speed - lastSpeed;
				
				int incremento = vf - speed;

				if(incremento != 0)
                        	{
				    T = lastT + incremento*0.2;

                            	    //if(a_car < 0)   T = lastT + 0.05;
                            	    //else            T = lastT - 0.05;
                        	}

			}

			
                        if(arrayCurves[pieceIndex] == 0) 
			{
				status = speedUp;
				 if(DEBUG_JESUS) printf("cambio de estado: %d -> speedUp(%d) \n", lastStatus, status);

				// TRATAR TURBO AQUI
				if(turboAvailable)
				{
					if(DEBUG_JESUS) printf("\nCHEQUEO DE TURBO\n");

					dTurbo = speed*ticksTurbo + 
						0.5*ticksTurbo*ticksTurbo*(T*factorTurbo - a_roz);

					if(dTurbo < arrayDistUntilChange[pieceIndex])
					    usaTurbo = 1;
				}
			}
			
                        break;
            }

	    if(T > 1.0)		T = 1.0;
	    else if(T < 0.0) 	T = 0.0;
            
	    if(usaTurbo)
	    {
		usaTurbo = 0;
		turboAvailable = 0;
		msg = turbo_msg();
	    }
	    else
	    {
            	msg = throttle_msg(T);
	    }            

            // Actualizar variables temporales
            lastPieceDistance = pieceDistance;
            lastPieceIndex = pieceIndex;
	    
	    last2Speed = lastSpeed;
	    lastSpeed = speed;            

	    last2CarAngle = lastCarAngle;
	    lastCarAngle = carAngle;

	    last2GameTick = lastGameTick;
	    lastGameTick = gameTick;

	    lastT = T;
	    lastStatus = status;
        } 
    
        else if (!strcmp("turboAvailable", msg_type_name)) {
            turboAvailable = 1;

             // pone
             factorTurbo = 0.0;
             ticksTurbo = 0.0;

            cJSON* _turboDurationTicks = NULL;
            cJSON* _turboFactor   = NULL;

	    _turboDurationTicks = cJSON_GetObjectItem(data, "turboDurationTicks");
	    _turboFactor = cJSON_GetObjectItem(data, "turboFactor");

            
             json_text = cJSON_Print(_turboDurationTicks);


            if (_turboDurationTicks) {
               ticksTurbo = atoi(json_text);
            } 

            json_text = cJSON_Print(_turboFactor);

            if (_turboFactor) {
               factorTurbo  = atof(json_text);
            }
        } 

        /*
        else if (!strcmp("crash", msg_type_name)) {

        }
        else if (!strcmp("gameEnd", msg_type_name)) {

        } */
	else if(!strcmp("spawn", msg_type_name)) {
		lastGameTick = gameTick;
		gameTick = cJSON_GetObjectItem(json, "gameTick")->valueint;
		status = speedUp;
		lastStatus = status;

		log_message(msg_type_name, json);
                msg = ping_msg();	
	}
        else if (!strcmp("gameInit", msg_type_name)) 
        {
            if (DEBUG_JESUS) printf("Inicio del JUEGO\n");

            // Al inicio analizamos el terreno
            status = speedUp;
	    computedRoz = 0;
            t0 = 0;

            /* Poner todo esto en una función para parsear  las partes de una pista */
            int i;
            cJSON* length = NULL;
            cJSON* itemSwitch = NULL;
            cJSON* radius = NULL;
            cJSON* angle = NULL;

            cJSON *race = cJSON_GetObjectItem(data, "race");
            cJSON *track = cJSON_GetObjectItem(race, "track");
            cJSON *pieces = cJSON_GetObjectItem(track, "pieces");
            cJSON *lanes = cJSON_GetObjectItem(track, "lanes");

            /* Lane Rules:
            The track may have 1 to 4 lanes. The lanes are described in the 
            protocol as an array of objects that indicate the lane's distance 
            from the track center line. A positive value tells that the lanes 
            is to the right from the center line while a negative value 
            indicates a lane to the left from the center.
            */

            /* ------------- inicio captura de lanes --------------------*/
            json_text = cJSON_Print(lanes);
               if (DEBUG_JOSE) printf("Mis Lanes: %s\n", json_text);

            nLanes = cJSON_GetArraySize(lanes);
            if (DEBUG_JOSE) printf("Circuito con %d lanes \n", nLanes);

            arrayLanes    = (float *) malloc(sizeof(float) * nLanes);

            /* bucle que recorre los lanes  */
            for (i = 0 ; i < nLanes ; i++)
            {
               cJSON * subitem = cJSON_GetArrayItem(lanes, i);
               int indexTmp = cJSON_GetObjectItem(subitem, "index")->valueint;
               arrayLanes[indexTmp] = (float) cJSON_GetObjectItem(subitem, "distanceFromCenter")->valueint;
               if (DEBUG_JOSE) printf("Lane index: %d  \n", indexTmp);    
          }
            
            if (DEBUG_JOSE) print_array_float(arrayLanes , sizeof(arrayLanes )/sizeof(arrayLanes[0]));

            /*----------------- fin captura de lanes  ---------------------*/ 

            nPieces = cJSON_GetArraySize(pieces);

            if (DEBUG_JESUS) printf("Circuito con %d piezas\n", nPieces);
 
            // Crear los arrays necesarios
            arrayAngles            = (float *) malloc(sizeof(float) * nPieces);
            arrayLengthAndRadius   = (float *) malloc(sizeof(float) * nPieces);
            arrayCurves            = (int *) malloc(sizeof(int) * nPieces);
            arraySwitches          = (int *) malloc(sizeof(int) * nPieces);
            arrayDistUntilChange   = (float *)  malloc(sizeof(float) * nPieces);
            arrayChangeIndex       = (int *)  malloc(sizeof(int) * nPieces);
            arrayChangeCurve       = (int *)  malloc(sizeof(int) * nPieces);

 
            /* bucle que recorre toda las piezas de una pista */
            for (i = 0 ; i < nPieces ; i++) 
            {
               cJSON * subitem = cJSON_GetArrayItem(pieces, i);
               length = cJSON_GetObjectItem(subitem, "length");
               itemSwitch = cJSON_GetObjectItem(subitem, "switch");
               radius = cJSON_GetObjectItem(subitem, "radius");
               angle  = cJSON_GetObjectItem(subitem, "angle");

               char * json_text = cJSON_Print(subitem);
               if (DEBUG_JESUS) printf("SubItem: %s\n", json_text);

               json_text = cJSON_Print(length);
               if (DEBUG_JESUS) printf("Length: %s\n", json_text);

               if (length) {
                  float _length = atof(json_text);
                  if (DEBUG_JESUS) printf("length: %f\n", _length);
                  arrayAngles[i] = 0.0;
                  arrayCurves[i] = 0;
                  arrayLengthAndRadius[i] = _length;
               }
               
               json_text = cJSON_Print(itemSwitch);
               if (DEBUG_JESUS) printf("Switch: %s\n", json_text);

               if(itemSwitch)   arraySwitches[i] = 1;
               else             arraySwitches[i] = 0;


               json_text = cJSON_Print(radius);
               if (DEBUG_JESUS) printf("Radius: %s\n", json_text);
               
               if (radius) 
               {
                  float _radius = atof(json_text);
                  if (DEBUG_JESUS) printf("radius: %f\n", _radius);
                  arrayCurves[i] = 1;
                  arrayLengthAndRadius[i] = _radius;
               }

               json_text = cJSON_Print(angle);
               if (DEBUG_JESUS) printf("Angle: %s\n", json_text);

               if (angle) 
               {
                  float floatAngle = atof(json_text);
                  if (DEBUG_JESUS) printf("floatAngle: %f\n", floatAngle);
                  arrayAngles[i] = floatAngle; 
               }
            }
           
            /* Definir el array arrayDistNextCurve*/
           
           // falta empezar por la pieza que empieza la recta o curva actual 
                                     
           int indexTrackAnalyzer = 0;
           // si es recta mirar el array hacia atrás hasta que se encuentra una curva
           // si es una curva, mirar hasta que se encuentra una recta o una curva    
           int indexMod = mod(indexTrackAnalyzer, (unsigned int) nPieces); 
          
           // Si la primera pieza es una recta
           if(arrayCurves[0] == 0)
           {
		// mientras seguimos teniendo rectas en piezas anteriores
		while (!arrayCurves[indexMod])
                {
			indexTrackAnalyzer--;
			indexMod = mod(indexTrackAnalyzer, (unsigned int) nPieces);
                        // if (DEBUG_JOSE) printf("indexMod: %d\n", indexTrackAnalyzer);
                }
           }
           else
           {
		// mientras seguimos teniendo igual piezas de curvas
                while(arrayCurves[indexMod] && arrayAngles[indexMod] == arrayAngles[0] && arrayLengthAndRadius[indexMod] == arrayLengthAndRadius[0]) {
                        if (DEBUG_JOSE) printf ("indexMod: %d\n", indexMod); 
              		indexTrackAnalyzer--;
                        indexMod = mod(indexTrackAnalyzer, (unsigned int) nPieces);
		} 
           }           
         
            // Aqui tenemos en indexTrackAnalyzer el índice de la pieza que
            // da comienzo a la recta o curva que cruza el punto de partida de la pista 
            
            // indexMod debe ser negativo. El bucle va desde el principio de curva o recta 
            // inicial hasta el final de esa curva o recta
            // Utilizamos dos indices: Uno que va desde indexMod (negativo) hasta nPieces
            // y el indice "i" que va tomado valores en el rango válid del array utilizando
            // el módulo. No es necesario poner nada en la inicialización del for porque
            // aquí indexTrackAnalyzer ya está inicializado al valor correcto
            indexTrackAnalyzer++;
 
            for (; indexTrackAnalyzer  < nPieces; indexTrackAnalyzer++) {
               i = mod(indexTrackAnalyzer, (unsigned int) nPieces);
               if (DEBUG_JOSE) printf ("i: %d\n", i );

               // Por cada pieza hacemos lo siguiente
               //    - Calculamos la distancia al siguiente cambio (curva, recta, angulo, radio)
               //      y la gurdamos en el arrayDist
               //    - Anotamos para esa pieza el cabio (recta o curva)
               //    - Anotamos índice de pieza en el que se produce el cambio
               // Necesitamos tres arrays arrayDist, arrayChangeType, arrayChangePieceIndex
               //
               int j = indexTrackAnalyzer; 
               float distUntilChange = 0.0;               
               int jMod = mod(j, (unsigned int) nPieces);
               // Mientras no haya cambios en el tipo de pieza
               while( (arrayCurves[jMod] == arrayCurves[i]) && 
                        (arrayAngles[jMod] == arrayAngles[i]) 
                      ) {
                 // Si estamos en una recta 
                  if (arrayCurves[jMod] == 0)  
                     distUntilChange = distUntilChange + (float) arrayLengthAndRadius[jMod];
                  else { // si estamos en curva arco = radio * Pi * abs(ang) /180
			 float angleTmp = arrayAngles[jMod];
                        if(angleTmp < 0) angleTmp = -angleTmp; // esto lo hacemos para evitar el problema
                                                               // con el valor absoluto que trunca el float
                        distUntilChange = distUntilChange + 
                                   arrayLengthAndRadius[jMod] * M_PI * 
                                   (angleTmp/180.0) ;
                  }
                  arrayDistUntilChange[jMod] = distUntilChange; 
                  j++;
                  jMod = mod(j, (unsigned int) nPieces);
                  // if (DEBUG_JOSE) printf("Angle: %f\n", arrayAngles[jMod] );
               }
               // Aquí tenemos el último ángulo y el último índice
               // hay que tener cuidado con jj que se incrementa al final
               // del bucle y es necesario restar una unidad para tener la última pieza
               
               // tenemos dos índices indexAnalyzer que indica la pieza que analizamos
               // y j que indica el lugar en el que se produce el cambio


               if (DEBUG_JOSE) printf("j: %d\n", j );
 
               arrayChangeIndex[indexTrackAnalyzer] = jMod; 
               if (DEBUG_JOSE) printf("jMod: %d\n", jMod );

           if (arrayCurves[jMod] == 1) // el cambio se produce en curva
                  arrayChangeCurve[jMod] = 1;
               else // el cambio se produce en recta 
                  arrayChangeCurve[jMod] = 0;

               if (DEBUG_JOSE) printf("arrayChangeCurve: %d\n", arrayChangeCurve[jMod]  );
    
               if (DEBUG_JOSE) printf("distanceUntilChange: %f\n", distUntilChange);
            } /* fin for */ 

            log_message(msg_type_name, json);
            msg = ping_msg();

            // cJSON_Delete(length);
            // cJSON_Delete(itemSwitch);
            // cJSON_Delete(radius);
            // cJSON_Delete(angle);

            // cJSON_Delete(race);
            // cJSON_Delete(track);
            // cJSON_Delete(pieces);
        } 
        else {
            log_message(msg_type_name, json);
            msg = ping_msg();

            if(DEBUG_JESUS) printf("\nPING!! \n\n");
        }

        write_msg(sock, msg);

        cJSON_Delete(msg);
        cJSON_Delete(json);
    }

    free(arrayAngles);
    free(arrayLengthAndRadius);
    free(arrayCurves);
    free(arraySwitches);
    free(arrayLanes);
   
    free(arrayDistUntilChange);  
    free(arrayChangeIndex);      
    free(arrayChangeCurve);      

    return 0;
}  /* --- end main ---- */

static cJSON *ping_msg()
{
    return make_msg("ping", cJSON_CreateString("ping"));
}

static cJSON *join_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON_AddStringToObject(data, "name", bot_name);
    cJSON_AddStringToObject(data, "key", bot_key);

    return make_msg("join", data);
}

static cJSON *throttle_msg(double throttle)
{
    return make_msg("throttle", cJSON_CreateNumber(throttle));
}

static cJSON *left(void) {
   cJSON *root;

   root = cJSON_CreateObject();

   cJSON_AddStringToObject(root, "msgType", "switchLane");
   cJSON_AddStringToObject(root, "data", "Left");

   return root;
}

static cJSON *right(void) {
   cJSON *root;

   root = cJSON_CreateObject();

   cJSON_AddStringToObject(root, "msgType", "switchLane");
   cJSON_AddStringToObject(root, "data", "Right");

   return root;
}

static cJSON *make_msg(char *type, cJSON *data)
{
    cJSON *json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "msgType", type);
    cJSON_AddItemToObject(json, "data", data);
    return json;
}

static cJSON *turbo_msg(void) {
    cJSON *root;
    root = cJSON_CreateObject();

    cJSON_AddStringToObject(root, "msgType", "turbo");
    cJSON_AddStringToObject(root, "data", "Bruuuum brumm turbo beat :)");
    return root;
}

static cJSON *read_msg(int fd)
{
    int bufsz, readsz;
    char *readp, *buf;
    cJSON *json = NULL;

    bufsz = 16;
    readsz = 0;
    readp = buf = malloc(bufsz * sizeof(char));

    while (read(fd, readp, 1) > 0) {
        if (*readp == '\n')
            break;

        readp++;
        if (++readsz == bufsz) {
            buf = realloc(buf, bufsz *= 2);
            readp = buf + readsz;
        }
    }

    if (readsz > 0) {
        *readp = '\0';
        json = cJSON_Parse(buf);
        if (json == NULL)
            error("malformed JSON(%s): %s", cJSON_GetErrorPtr(), buf);
    }
    free(buf);
    return json;
}

static void write_msg(int fd, cJSON *msg)
{
    char nl = '\n';
    char *msg_str;

    msg_str = cJSON_PrintUnformatted(msg);

    write(fd, msg_str, strlen(msg_str));
    write(fd, &nl, 1);
    if (DEBUG_MSG_TO_SERVER) printf("%s\n", msg_str); 
    free(msg_str);
}
