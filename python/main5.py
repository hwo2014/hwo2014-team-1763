import json
import socket
import sys
import time
from track import *  
from array import *
import math

DIV = 80 # Keimola
         # 80 -> 8:35   (with 2 pieces view)
         # 90 -> out in 1 and 3 (with 2 pieces view)
         # 80 -> with 3 pices view 8:33
         # 80 -> with 4 pieces view 8:27 
         # 82 -> with 4 pieces view 8:15
         # 85 -> with 4 pieces view 8:02
         # 87 -> with 4 pieces view 7:97 
         # 88 -> with 4 pieces view 8:00 brake 4:0.0, 15:0.2
         # 90 -> with 4 pieces view 8:10 brake 4:0.0, 7,15:0.2
         # 97 -> with 4 pieces view 8:02 brake 3:0-0 6,15,32:0.2 
         # 100 -> with 4 pieces view 8:02 brake 3:0-0 6,15,32:0.2 
         # 105 -> with 4 pieces view 8:03 brake 3,15:0.0 6,32,27:0.2

SWITCH = 0
DEBUG = 1
INITIAL_SPEED = 0.65 

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.start_time = 0 
        self.turn_time = 0
        self.turn_start_time = 0
        self.lap_time = 0
        self.turbo = 0
        self.pieces = None 
        self.gameTick = 0 
        self.angles = array('f', [])
        self.radius = array('f', [])
        self.lengths = array('d', [])       
        self.throttles = array('f', []) 
        self.switches = {}     
        self.lap = 0 
        self.speeds = {}
        self.carAngles = {} 
        self.pieceIndex = 0
        self.pieceDistance = 0
        self.pieceStartPosition = 0
        self.pieceI = 0
        self.carAngle = 0 
        self.throttlesDict = {}
 
    def turboAction(self, data):
        self.turbo = 1

    def time_remaining(self):
        return self.turntime - int(1000 * (time.time() - self.turn_start_time))

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def left(self):
        self.send(json.dumps({"msgType": "switchLane", "data": "Left"}))

    def right(self):
        self.send(json.dumps({"msgType": "switchLane", "data": "Right"}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def createRace(self, data):
        self.send(json.dumps({"msgType": "createRace", "data": { "botId": { "name": self.name, "key": self.key }, "trackName": "france", "carCount": 1}}))

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        print("Throttle:")
        print(throttle)
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.createRace("")
        # self.join()
        self.turn_start_time = time.time()
        if DEBUG:
            print("Start time: ".format(self.turn_start_time))
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_game_init(self, data):
        print("Game init")
        if DEBUG: 
            print(data)
        self.pieces = data["race"]["track"]["pieces"]
        for i in self.pieces:
            if 'length' in i:
                self.angles.append(0.0)
                self.radius.append(99999999999.999)
                self.lengths.append(i['length'])
            else:
                self.radius.append(i['radius'])
                self.angles.append(i['angle'])
                angle  =  i['angle']
                radius =  i['radius']
                length = 2* radius* math.pi *  (abs(angle)/360) 
                self.lengths.append(length)
        self.ping()
        # print(self.angles)
        # print(self.radius)
        # print(self.lengths)

    def on_car_positions(self, data):
        self.throttle(0.4)
        return

        pieceIndex = data[0]["piecePosition"]["pieceIndex"]
        self.carAngle = data[0]["angle"]
        if (pieceIndex != self.pieceIndex): # nueva parte
            self.pieceIndex = data[0]["piecePosition"]["pieceIndex"]
            self.pieceI = 0
            self.pieceDistance = 0
            self.pieceStartPosition = data[0]["piecePosition"]["inPieceDistance"]
        else:
            self.pieceI = self.pieceI + 1; 
            self.pieceDistance =  data[0]["piecePosition"]["inPieceDistance"] - self.pieceStartPosition
           #  print(self.pieceDistance)

        # print(self.pieces[pieceIndex])
        self.lap = data[0]["piecePosition"]["lap"]

        # if pieceIndex in [3,15]: # Final de recta
        #   self.throttle(0.0)
        #   return

        #if pieceIndex in [6,32,27]: # Final de recta
        #   self.throttle(0.2)
        #   return



        mediumAngle = 0
        absMediumAngle = 0
   
        
        key = str(self.lap) +  '-' + str(pieceIndex) + "-" + str(self.pieceI) 
        # print(key)
        if self.pieceI != 0:
            self.speeds[key] = self.pieceDistance / self.pieceI
        else:
            self.speeds[key] = None
        print("speed:")
        print(self.speeds[key])

        self.carAngles[key] = self.carAngle
        print("carAngle:")
        print(self.carAngles[key])

        # if 'switch' in self.pieces[(pieceIndex+1) % len(self.pieces)] and not key in self.switches:

        # if 'switch' in self.pieces[(pieceIndex+1) % len(self.pieces)]:

        # self.switches[key] = 1           
        newIndex1 = (pieceIndex+2) % len(self.pieces)
        newIndex2 = (pieceIndex+3) % len(self.pieces)
        newIndex3 = (pieceIndex+4) % len(self.pieces)
        newIndex4 = (pieceIndex+5) % len(self.pieces)
        newIndex5 = (pieceIndex+5) % len(self.pieces)
           
        if 'angle' in self.pieces[newIndex1]:
            mediumAngle = self.pieces[newIndex1]['angle']
            absMediumAngle = abs(self.pieces[newIndex1]['angle'])

        if 'angle' in self.pieces[newIndex2]:
            mediumAngle = mediumAngle + self.pieces[newIndex2]['angle']
            absMediumAngle = absMediumAngle + abs(self.pieces[newIndex2]['angle'])

        if 'angle' in self.pieces[newIndex3]:
           mediumAngle = mediumAngle + self.pieces[newIndex3]['angle']
           absMediumAngle = absMediumAngle + abs(self.pieces[newIndex3]['angle'])

        if 'angle' in self.pieces[newIndex4]:
            mediumAngle = mediumAngle + self.pieces[newIndex4]['angle']
            absMediumAngle = absMediumAngle + abs(self.pieces[newIndex4]['angle'])
 
        # if 'angle' in self.pieces[newIndex5]:
            # mediumAngle = mediumAngle + self.pieces[newIndex5]['angle']
            # absMediumAngle = absMediumAngle + abs(self.pieces[newIndex5]['angle'])

        # if 'switch' in self.pieces[(pieceIndex+1) % len(self.pieces)]:

        if 'switch' in self.pieces[(pieceIndex+1) % len(self.pieces)] and not key in self.switches:

           self.switches[key] = 1           
           newIndex1 = (pieceIndex+2) % len(self.pieces)
           if mediumAngle > 0:
               # giro a derecha
               self.right()
               # print("Derecha!")
               # print("mediumAngle:")
               # print(mediumAngle)
               self.ping()
               return 
           elif mediumAngle <0:
               self.left()
               # print("Izquierda!")
               # print("mediumAngle")
               # print(mediumAngle)
               self.ping()
               return
           # else:
               # print("mediumAngle")
               # print(mediumAngle)

        # length = len(self.throttles)
        # if length == 0:
        #    self.throttle(INITIAL_SPEED)
        #    self.throttles.append(INITIAL_SPEED)      
        #    return
 
        # throttle = 1.0 - (1.0 / abs(mediumAngle))
        print("absMediumAngle")
        print(absMediumAngle)
        throttle = 1.0 - (absMediumAngle/(4*DIV))

        if throttle > 1.0:
           throttle = 1.0
        if throttle < 0.0:
           throttle = 0.0

        self.throttles.append(throttle)      
        self.throttle(throttle)

        self.throttlesDict[key] = self.throttle 

        print("Throttle") 
        print(throttle)
        self.ping()
        return       
 
    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'createRace': self.createRace,
            'gameInit': self.on_game_init, 
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'right': self.right,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'turboAvailable': self.turboAction,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            if (DEBUG):
                print("Line: ")
                print(line) 
            msg = json.loads(line)
            if (DEBUG):
                print("Msg: ")
                print(msg)
            msg_type, data  = msg['msgType'], msg['data']
            if 'gameTick' in msg:
                self.gameTick = msg['gameTick']
                # print(self.gameTick)
            if msg_type in msg_map:
                self.lap_time = time.time()
                if (DEBUG): 
                    print("Lap time: ")
                    print(self.lap_time)
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
