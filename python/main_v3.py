import json
import socket
import sys
import time
from track import *  

DEBUG = 0
VEL_RECTA = 0.7
VEL_CURVA = 0.6 

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.start_time = 0 
        self.turn_time = 0
        self.turn_start_time = 0
        self.lap_time = 0
        self.turbo = 0
        self.pieces = None
        self.gameTick = 0 

    def turboAction(self, data):
        self.turbo = 1

    def time_remaining(self):
        return self.turntime - int(1000 * (time.time() - self.turn_start_time))

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def left(self):
        self.send(json.dumps({"msgType": "switchLane", "data": "Left"}))

    def right(self):
        self.send(json.dumps({"msgType": "switchLane", "data": "Right"}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.turn_start_time = time.time()
        if DEBUG:
            print("Start time: ".format(self.turn_start_time))
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_game_init(self, data):
        print("Game init")
        if DEBUG: 
            print(data)
        self.pieces = data["race"]["track"]["pieces"]
        self.ping()


    def on_car_positions3(self, data):
        pieceIndex = data[0]["piecePosition"]["pieceIndex"] 
        inPieceDistance = data[0]["piecePosition"]["inPieceDistance"]
        angle = data[0]["angle"]

    def on_car_positions2(self, data):
        # http://www.reddit.com/r/HWO/comments/23m3ze/simple_logic_for_a_793_second_keimola_lap_time/
        pieceIndex = data[0]["piecePosition"]["pieceIndex"]
        inPieceDistance = data[0]["piecePosition"]["inPieceDistance"]

        if (self.turbo) and (pieceIndex in [34]) and inPieceDistance > 40:
            print("Turbo!") 
            self.send(json.dumps({"msgType": "turbo", "data": "Turbo!"}))
            self.turbo = 0
            return

        # if (self.turbo) and pieceIndex in [4,5,6]:
        #    self.throttle(0.1) 
        #    return

        if pieceIndex in [34,35,36,37,38,39,0,1,7,8,9,10,11,22,23,24]:
            self.throttle(1.0)
        elif pieceIndex in [31,32,33]:
            self.throttle(0.8)
        elif pieceIndex in [4,5,6,18,19,20,21]:
            self.throttle(0.7)
        elif pieceIndex in [13,14,15,16,17,26,27,28,29,30]:
            self.throttle(0.6)
        else:
            self.throttle(0.0)
        self.ping()
        return

    def on_car_positions(self, data):
        #  if 'lane' in data[0]["piecePosition"]:
        # g2["data"][0]["piecePosition"]["pieceIndex"] 
        pieceIndex = data[0]["piecePosition"]["pieceIndex"]

        # Empezamos a todo gas  
        if (self.gameTick in [0,1,2,3]):
            self.throttle(1.0)
            self.ping()
            return         
        if (pieceIndex == 4):
            self.right()
            self.ping()
            return
        if (pieceIndex == 19):
            self.left()
            self.ping()
            return
        if 'length' in self.pieces[(pieceIndex+1) % (len(self.pieces)-1)]: 
            if (DEBUG): 
                print("Proximo tramo en recta!")
            # If a long lane starts
            if (pieceIndex in [35,36,37]):
                self.throttle(1.0) #principio de recta, maxima velocidad 
                if (self.turbo) and (pieceIndex in [35,36,37]):
                    # if (DEBUG): 
                    print("Turbo!") 
                    # self.send(json.dumps({"msgType": "turbo", "data": "Turbo!"}))
            else:
                self.throttle(VEL_RECTA) # recta normal     
        else:
            # final de una recta larga
          
            if (pieceIndex in [3,4,5]) and self.turbo:
                self.throttle(0.1)
                self.turbo = 0
            else: 
               self.throttle(VEL_CURVA)
               # self.throttle(0.6567)
        self.ping()  
    
    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init, 
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions2,
            'right': self.right,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'turboAvailable': self.turboAction,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            if (DEBUG):
                print("Line: ")
                print(line) 
            msg = json.loads(line)
            if (DEBUG):
                print("Msg: ")
                print(msg)
            msg_type, data  = msg['msgType'], msg['data']
            if 'gameTick' in msg:
                self.gameTick = msg['gameTick']
                # print(self.gameTick)
            if msg_type in msg_map:
                self.lap_time = time.time()
                if (DEBUG): 
                    print("Lap time: ")
                    print(self.lap_time)
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
