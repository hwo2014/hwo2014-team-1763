import json
import socket
import sys
import time
from track import *  
from array import *
import math

SWITCH = 0
DEBUG = 0
INITIAL_SPEED = 0.6
K1 = 0.7
K2 = 0.2
MAX_ANGLE = 65

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.start_time = 0 
        self.turn_time = 0
        self.turn_start_time = 0
        self.lap_time = 0
        self.turbo = 0
        self.pieces = None 
        self.gameTick = 0 
        self.angles = array('f', [])
        self.radius = array('f', [])
        self.lengths = array('d', [])       
        self.throttles = array('f', []) 
        self.switches = {}     
        self.lap = 0 
 
    def turboAction(self, data):
        self.turbo = 1

    def time_remaining(self):
        return self.turntime - int(1000 * (time.time() - self.turn_start_time))

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def left(self):
        self.send(json.dumps({"msgType": "switchLane", "data": "Left"}))

    def right(self):
        self.send(json.dumps({"msgType": "switchLane", "data": "Right"}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.turn_start_time = time.time()
        if DEBUG:
            print("Start time: ".format(self.turn_start_time))
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_game_init(self, data):
        print("Game init")
        if DEBUG: 
            print(data)
        self.pieces = data["race"]["track"]["pieces"]
        for i in self.pieces:
            if 'length' in i:
                self.angles.append(0.0)
                self.radius.append(99999999999.999)
                self.lengths.append(i['length'])
            else:
                self.radius.append(i['radius'])
                self.angles.append(i['angle'])
                angle  =  i['angle']
                radius =  i['radius']
                length = 2* radius* math.pi *  (abs(angle)/360) 
                self.lengths.append(length)
        self.ping()
        # print(self.angles)
        # print(self.radius)
        # print(self.lengths)

    def on_car_positions(self, data):
        pieceIndex = data[0]["piecePosition"]["pieceIndex"]
        # print(self.pieces[pieceIndex])
        self.lap = data[0]["piecePosition"]["lap"]


        key = str(self.lap) +  '-' + str(pieceIndex) 

        if 'switch' in self.pieces[pieceIndex] and not key in self.switches:

           self.switches[key] = 1           
           newIndex1 = (pieceIndex+1) % len(self.pieces)
           newIndex2 = (pieceIndex+2) % len(self.pieces)
           newIndex3 = (pieceIndex+3) % len(self.pieces)
           newIndex4 = (pieceIndex+4) % len(self.pieces)
           
           mediumAngle = 0
           if 'angle' in self.pieces[newIndex1]:
               mediumAngle = self.pieces[newIndex1]['angle']

           if 'angle' in self.pieces[newIndex2]:
               mediumAngle = mediumAngle + self.pieces[newIndex2]['angle']
 
           if 'angle' in self.pieces[newIndex3]:
               mediumAngle = mediumAngle + self.pieces[newIndex3]['angle']

           if 'angle' in self.pieces[newIndex4]:
               mediumAngle = mediumAngle + self.pieces[newIndex4]['angle']
 
           if mediumAngle > 0:
               # giro a derecha
               self.right()
               print("Derecha!")
               print(mediumAngle)
               self.ping()
               return 
           elif mediumAngle <0:
               self.left()
               print("Izquierda!")
               print(mediumAngle)
               self.ping()
               return
           else:
               print(mediumAngle)

        length = len(self.throttles)
        if length == 0:
            self.throttle(INITIAL_SPEED)
            return
        elif length < 5:
            throttle = self.throttles[length-1] + K1 * (MAX_ANGLE - abs(self.angles[pieceIndex])) - K2 * (1/self.radius[(pieceIndex+2) % len(self.radius)]) 

        else:
            throttle = sum(self.throttles[-4:])/4 + K1 * (MAX_ANGLE - abs(self.angles[pieceIndex])) - K2 * (1/self.radius[(pieceIndex+3) % len(self.radius)]) 

        if throttle > 1:
           throttle = 1.0
        if throttle < 0:
           throttle = 0
        self.throttles.append(throttle)      
        self.throttle(throttle)
       
 
    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init, 
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'right': self.right,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'turboAvailable': self.turboAction,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            if (DEBUG):
                print("Line: ")
                print(line) 
            msg = json.loads(line)
            if (DEBUG):
                print("Msg: ")
                print(msg)
            msg_type, data  = msg['msgType'], msg['data']
            if 'gameTick' in msg:
                self.gameTick = msg['gameTick']
                # print(self.gameTick)
            if msg_type in msg_map:
                self.lap_time = time.time()
                if (DEBUG): 
                    print("Lap time: ")
                    print(self.lap_time)
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
