# Class Track 

class Track:
    def __init__(self,x,y):
        self.x = x
        self.y = y
        self.num_pieces = 0
        self_max_radius = 0
        self_max_length = 0
        self_num_switch = 0

    description = "This is Track class"
    author = "Jose Carpio-Canada"

    def area(self):
        return self.x * self.y

    def perimeter(self):
        return 2 * self.x + 2 * self.y

    def describe(self,text):
        self.description = text

    def authorName(self,text):
        self.author = text

    def scaleSize(self,scale):
        self.x = self.x * scale
        self.y = self.y * scale
